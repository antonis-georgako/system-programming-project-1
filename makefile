HEADERS = myheader.h
SOURCE = minisearch.c arg_check.c id_map.c trie_fill.c tf.c df.c exit_map.c exit_trie.c
OBJECTS = minisearch.o arg_check.o id_map.o trie_fill.o tf.o df.o exit_map.o exit_trie.o
CC = gcc
CFLAGS = -Wall -g

all: minisearch

#separate compilation

minisearch: $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) -o minisearch	

	

minisearch.o: minisearch.c $(HEADERS)
	$(CC) $(CFLAGS) -c minisearch.c

arg_check.o: arg_check.c $(HEADERS)
	$(CC) $(CFLAGS) -c arg_check.c

id_map.o: id_map.c $(HEADERS)
	$(CC) $(CFLAGS) -c id_map.c

trie_fill.o: trie_fill.c $(HEADERS)
	$(CC) $(CFLAGS) -c trie_fill.c

tf.o: tf.c $(HEADERS)
	$(CC) $(CFLAGS) -c tf.c

df.o: df.c $(HEADERS)
	$(CC) $(CFLAGS) -c df.c

exit_map.o: exit_map.c $(HEADERS)
	$(CC) $(CFLAGS) -c exit_map.c

exit_trie.o: exit_trie.c $(HEADERS)
	$(CC) $(CFLAGS) -c exit_trie.c






clean:
	-rm $(OBJECTS)
	-rm minisearch

count:
	wc $(SOURCE) $(HEADERS)
