#ifndef MYHEADER
#define MYHEADER
#include <stdio.h>

//domh gia to map
struct map{
	int doc_id;
	char * line;
	struct map *next;
	
};

typedef struct map * map_node;



//domh gia to posting list
struct posting_list{
	int doc_id;
	int num_of_appear;
	struct posting_list * next;
};

typedef struct posting_list * posting_node;





//domh gia to trie
struct trie{
	char c;
	struct trie *next; //pame dipla
	struct trie *previous; //deixnei ston prohgoumeno
	struct trie *down; //pame katw (idia leksi)
	struct posting_list * post_head;
};

typedef struct trie * trie_node;






int trie_fill(map_node *,trie_node *);

int arg_check(int argc,char * argv[]);

int id_map(map_node *);

void tf(int , char*, trie_node *);

void df(trie_node*, char*);

void exit_map(map_node *);

void exit_trie(trie_node *);


extern FILE *docfile;
extern int k; //default timh ama den m dwsei K



#endif
