#include <stdio.h>
#include <stdlib.h>
#include "myheader.h"

void exit_trie(trie_node *headtrie){

	trie_node current_trie;
	current_trie = *headtrie;
	if(current_trie->down != NULL)
		exit_trie(&(current_trie->down));
	if(current_trie->post_head !=NULL){ //diagrafh posting list
		posting_node temp;
		temp = current_trie->post_head;
		while((current_trie->post_head)!=NULL){
			temp = current_trie->post_head;
			current_trie->post_head = (current_trie->post_head)->next;
			free(temp);
		}
	}
	if(current_trie->next != NULL)
		exit_trie(&(current_trie->next));
	free(current_trie);



	return;
}
