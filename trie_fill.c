#include <stdio.h>
#include <stdlib.h>
#include "myheader.h"



//synarthsh gia na gemisoume to trie me to map pou exoume ftiaxei hdh
int trie_fill(map_node *headmap, trie_node *headtrie){


	//ksekiname me adeio headtrie ara pame na kanoume allocate prwto kombo
	if(headmap == NULL){
		printf("Empty map! Exiting...\n");
		return 1;
	}

	
	//gia to trie
	trie_node current_trie; //kombos gia allages sto trie ktl gia na mhn xasoume to root
	*headtrie = (trie_node)malloc(sizeof(struct trie)); //desmeyoume gia prwto kombo
	current_trie = *headtrie;
	trie_node* current_head; //proswrinos kombos pou mou deixnei panta poio einai to "kefali" apo to
							//opoio proerxomai
	trie_node previous_node; //mou deixnei ton prohgoumeno kombo


	//gia to map
	map_node current_map; //proswrinos gia to map gia na paroume tis lekseis tou doc
	current_map = *headmap;
	int i=0; // metrhths gia ton pinaka tou map dld me tis grammes



	//xarakthras : metablhth pou deixnei poio gramma tha mpei sto trie kathe fora. dld einai ta grammata pou exei to map
	int xarakthras = current_map->line[i];
	int prev = ' '; //arxika leme oti to "prohgoumeno" gramma pou epeksergasthkame htan keno
					//dld me alla logia ksekiname me nea leksi

	int flag=1; //metablhth gia thn anazhthsh trie komboy. an brw kombo pou exei to idio gramma tote flag=0;
				//epishs an paw na balw endiameso kombo me gramma mikrotero apo tou epomenou tote epishs flag=0;
				//an sthn anazhthsh gia kombo den brw kanenan tote flag=1;


	while(1){
		if(xarakthras == ' ' || xarakthras == '\t'){
			if((prev != ' ' && prev != '\t')){ //shmainei oti prin eixame xarakthra, ara twra exei teleiwsei kapoia leksi


			

				//ftiaxnoume posting_list gia thn leksi pou molis teleiwse
				if(current_trie->post_head == NULL){
					posting_node temp;
					temp = (posting_node)malloc(sizeof(struct posting_list));

					current_trie->post_head = temp;
					(current_trie->post_head)->doc_id = current_map->doc_id;
					(current_trie->post_head)->num_of_appear = 1;
					
				}else{


					posting_node current_post;
					current_post = current_trie->post_head;


					//an yparxei hdh posting list pame na doume an yparxei kombos gia to id pou eimaste
					while((current_post)->doc_id != current_map->doc_id){
						
						if((current_post)->next == NULL){

							break;
						}
						current_post = current_post->next;
					}






					if((current_post)->doc_id == current_map->doc_id){
						(current_post)->num_of_appear = ((current_post)->num_of_appear) + 1;

					}else{



						posting_node temp;
						temp = (posting_node)malloc(sizeof(struct posting_list));
						(current_post)->next = temp;
						current_post = current_post->next;

						(current_post)->doc_id = current_map->doc_id;
						(current_post)->num_of_appear = 1;
					}


				



				}



			}
			current_trie = *headtrie; //gyrname pali sthn koryfh tou trie
			prev = xarakthras;


		}else{ // an exoume xarakthra kai oxi keno h tab

			////////           1o EPIPEDO            ////////////
			if(prev == ' ' || prev == '\t'){ //an einai to prwto gramma kapoias leksis
											

				current_trie = *headtrie;



				//eimaste sto head tou trie ara:
				while(current_trie != NULL){ //enosw yparxei desmeymenos kombos

					//se periptwsh pou exw desmeysei kai DEN yparxei xarakthras (1os kombos kefalhs), dld 1h epanalhpsh
					if((current_trie->c) == NULL){ 
						current_trie->c = xarakthras;
						flag = 0;
						break; 
					}else if((current_trie->c) == xarakthras){ //an yparxei hdh mesa apla bgainoume
						flag = 0;
						break;
					}else if((current_trie->c) > xarakthras){
						trie_node temp; //o neos "endiamesos" kombos
						temp = (trie_node)malloc(sizeof(struct trie));
						temp->c = xarakthras;
						temp->next = current_trie; //enwnw temp me current
						if(current_trie->previous != NULL){ //an yparxei o prohgoymenos tou current
							trie_node temp3;
							temp3 = current_trie->previous;
							temp3->next = temp;
							temp->previous = temp3;   
						}else{ //alliws an prepei na mpei ws prwtos kombos
							*headtrie = temp;


						}
						current_trie->previous = temp;
						current_trie = current_trie->previous;         
						flag = 0;
						break;
					}
					previous_node = current_trie;
					current_trie = current_trie->next;
				}//telos while,

				//twra pou teleiwse to while exoume 2 senaria: 1) balame ton kombo kapou sto epipedo
				// kai 2) den ton balame ara mpainei sto telos

				if(flag != 0){ //se periptwsh pou prepei na mpei sto telos o kombos
								
					current_trie = (trie_node)malloc(sizeof(struct trie));
					current_trie->c = xarakthras;
					current_trie->previous = previous_node; //enwnoume ton current me prohgoumeno
					previous_node->next = current_trie; //enwnoume previous me current
					
				}
				flag = 1; //epanaferoume to flag

				prev = xarakthras;




			}else{ //an o prev DEN einai keno. Eimaste se 2o h pio xamhlo "epipedo"


				trie_node temp22;
				temp22 = current_trie;
				current_head = &temp22;



				if(current_trie->down == NULL){ //an DEN yparxei hdh katw kombos tote pame na ftiaxoume 
					trie_node temp;

					temp = (trie_node)malloc(sizeof(struct trie));
					current_trie->down = temp;
					current_trie = current_trie->down;
					current_trie->c = xarakthras;

					//ara exoume "katebei" ena epipedo kai exoume apothikeysei ton xarakthra mas


				}else{ // an YPARXEI o katw kombos tote pame kai psaxnoyme gia kombous me idio h megalytera
						//grammata opws kaname panw gia na kseroume pou tha baloume to gramma mas


					current_trie = current_trie->down;


						//eimaste sto head tou trie ara:
					while(current_trie != NULL){ //enosw yparxei desmeymenos kombos



						//se periptwsh pou exw desmeysei kai DEN yparxei xarakthras (1os kombos kefalhs)
						if((current_trie->c) == NULL){
							current_trie->c = xarakthras;
							flag = 0;
							break; 
						}else if((current_trie->c) == xarakthras){
							flag = 0;
							break;
						}else if((current_trie->c) > xarakthras) {


							trie_node temp; //o neos "endiamesos" kombos
							temp = (trie_node)malloc(sizeof(struct trie));
							temp->c = xarakthras;
							temp->next = current_trie; //enwnw temp me current
							if(current_trie->previous != NULL){ //an yparxei o prohgoymenos tou current
								trie_node temp3;
								temp3 = current_trie->previous;
								temp3->next = temp;
								temp->previous = temp3;   
							}else{

								((*current_head)->down) = NULL;
								((*current_head)->down) = temp;
								
							}
							
							current_trie->previous = temp;
							current_trie = temp;         
							flag = 0;
							break;


						}
						previous_node = current_trie;
						current_trie = current_trie->next;
					}//telos while, psaksimo sto trie

					//twra pou teleiwse to while exoume 2 senaria: 1) balame ton kombo kapou sto epipedo
					// kai 2) den ton balame ara mpainei sto telos

					if(flag != 0){ //se periptwsh pou prepei na mpei sto telos o kombos
									

						current_trie = (trie_node)malloc(sizeof(struct trie));
						current_trie->c = xarakthras;
						current_trie->previous = previous_node; //enwnoume ton current me prohgoumeno
						previous_node->next = current_trie; //enwnoume previous me current
						
					}
					flag = 1; //epanaferoume to flag

					prev = xarakthras;
				}
				prev = xarakthras;
			}
			prev = xarakthras;
		} //telos elegxou xarakthrwn
		


		//gia thn allagh tou grammatos.
		i++;
		
		if(current_map->line[i] == '\0'){
			if(current_map->next != NULL){
				current_map = current_map->next;
			}else{ //an eimaste ston teleytaio xarakthra ths teleytaias seiras tou map tote bgainoume
				break;
			}
			i=0;
			prev = ' ';
		}
		
		xarakthras = current_map->line[i];


	}//gia to while dld oso akoma exoume map


	return 0;
}


