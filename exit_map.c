#include <stdio.h>
#include <stdlib.h>
#include "myheader.h"

void exit_map(map_node * headmap){

	map_node prev;
	map_node temp;
	temp = *headmap;
	while(temp != NULL){
		free(temp->line);
		prev = temp;
		temp = temp->next;
		free(prev);

	}

	printf("Map freed\n");



	return;
}
